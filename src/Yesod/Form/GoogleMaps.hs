{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Yesod.Form.GoogleMaps where

import ClassyPrelude.Yesod

addPlaceId :: FieldSettings site -> FieldSettings site
addPlaceId = withAdditionalClass "place_id"

addLocationField :: FieldSettings site -> FieldSettings site
addLocationField = withAdditionalClass "locationField"

addStreetNumber :: FieldSettings site -> FieldSettings site
addStreetNumber = withAdditionalClass "street_number"

addRoute :: FieldSettings site -> FieldSettings site
addRoute = withAdditionalClass "route"

addPostalCode :: FieldSettings site -> FieldSettings site
addPostalCode = withAdditionalClass "postal_code"

addLocality :: FieldSettings site -> FieldSettings site
addLocality = withAdditionalClass "locality"

addAdministrativeAreaLevel1 :: FieldSettings site -> FieldSettings site
addAdministrativeAreaLevel1 = withAdditionalClass "administrative_area_level_1"

addcountry :: FieldSettings site -> FieldSettings site
addcountry = withAdditionalClass "country"

-- | Add the 'cls' CSS class to a field.
withAdditionalClass :: Text -> FieldSettings site -> FieldSettings site
withAdditionalClass cls fs = fs {fsAttrs = newAttrs}
  where
    newAttrs = addClass cls (fsAttrs fs)

-- | You have to provide input fields with the followig classes:
--  * place_id
--  * street_number
--  * route
--  * postal_code
--  * locality
--  * administrative_area_level_1
--  * country
--  These fields will be populated with the autocomplete suggestions.
--  Also provide a text input with class "locationField" where the user enters her address.
--  This widget provides the JS code for this to work.
addressAutoComplete :: Text -> WidgetFor site ()
addressAutoComplete accessKey = do
  toWidgetHead [hamlet|<script src="https://cdn.plot.ly/plotly-latest.min.js">|]
  toWidgetBody
    [julius|
        var placesService, autocomplete, map, marker;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'short_name',
            postal_code: 'short_name'
        };

        function initMap() {
            // The location of Uluru
            var uluru = {lat: -25.344, lng: 131.036};
            // The map, centered at Uluru
            map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: uluru});
            // The marker, positioned at Uluru
            marker = new google.maps.Marker({position: uluru, map: map});
        }

        function refocusMap() {
            var place = autocomplete.getPlace();
            refocusWithPlace(place, google.maps.places.PlacesServiceStatus.OK);
        }

        function refocusWithPlace(place, status) {
            console.info("refocusWithPlace", place, status);
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                if (place.geometry) {
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(15);
                    }
                    marker.setPlace({
                        placeId: place.place_id,
                        location: place.geometry.location
                    });
                    marker.setVisible(true);
                } else {
                    console.warn("No place geometry.")
                }
            } else {
                console.warn("Places status is not OK!");
            }
        }

        function disableAddressComponentFields() {
            for (var component in componentForm) {
                var flds = document.getElementsByClassName(component);
                for (var i=0; i<flds.length; i++) {
                    console.info("Clear field", component, "with old value", flds[i].value);
                    flds[i].readonly = true;
                }
            }
        }

        function initAutocomplete() {
            disableAddressComponentFields();

            document.getElementsByClassName("locationField")[0].addEventListener("focus", geolocate);

            initMap();

            placesService = new google.maps.places.PlacesService(map);
            var thePlaceId = document.getElementsByClassName("place_id")[0].value;
            if (thePlaceId) {
                placesService.getDetails(
                    {
                        placeId: thePlaceId,
                        fields: ['name', 'formatted_address', 'place_id', 'geometry']
                    },
                    refocusWithPlace);
            }

            // Create the autocomplete object, restricting the search predictions to
            // geographical location types.
            // maybe change geocode to address?
            autocomplete = new google.maps.places.Autocomplete(document.getElementsByClassName("locationField")[0], {types: ['geocode']});
            // autocomplete.bindTo('bounds', map);

            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the necessary components.
            autocomplete.setFields(['address_component', 'geometry', 'place_id']);

            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
            autocomplete.addListener('place_changed', refocusMap);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            console.info("The place from autocomplete: ", place);
            for (var component in componentForm) {
                var flds = document.getElementsByClassName(component);
                for (var i=0; i<flds.length; i++) {
                    console.info("Clear field", component, "with old value", flds[i].value);
                    flds[i].value = '';
                }
            }
            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                console.info("Found address type ", addressType);
                if (componentForm[addressType]) {
                    var flds = document.getElementsByClassName(addressType);
                    var val = place.address_components[i][componentForm[addressType]];
                    for (var j=0; j<flds.length; j++) {
                        flds[j].value = val;
                    }
                }
            }
            var place_id_flds = document.getElementsByClassName("place_id");
            for (var j=0; j<place_id_flds.length; j++) {
                console.info("Set place_id to", place.place_id);
                place_id_flds[j].value = place.place_id;
            }
        }
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    console.info("The users current position: ", position);
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    |]
  toWidget
    [whamlet|<script src="https://maps.googleapis.com/maps/api/js?key=#{accessKey}&libraries=places&callback=initAutocomplete" async defer>|]
